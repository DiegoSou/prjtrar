package control.db;


import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class ConexaoDb {
	
	private static Connection conn = null;

	public static Connection getConn() {
		return conn;
	}
	public static void setCon(Connection c) {
		if(c!=null) {
			conn = c;
		}
	}
	public static boolean abrirConexao() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String user = "diegosous";
			String password = "12345678";
			String banco = "prjtSoft";
			String url = "jdbc:mysql://localhost/" + banco;
			setCon(DriverManager.getConnection(url, user, password));
			return true;
                        
		}catch (ClassNotFoundException e){
			System.out.println("Erro arquivo de configuração não existente!");
			System.out.println(e.getMessage());
			return false;
		}catch(SQLException e) {
			System.out.println("Erro ao conectar ao banco!");
			System.out.println(e.getMessage());
			return false;
		}
	}
	

	public static boolean fecharConexao() {
		try {
			conn.close();
			return true;
		}
		catch (SQLException e) {
			System.out.println("Conexão não pode ser fechada!");
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public static Properties carregarProps() throws IOException{
		FileInputStream fs = new FileInputStream("db.properties");
		Properties props = new Properties();
		props.load(fs);
		return props;
	}
	
	public static void fecharStatement(PreparedStatement st) {
		try {
			st.close();
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}
	
	public static void fecharResultSet(ResultSet rs) {
		try {
			rs.close();
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		
	}
	
}
