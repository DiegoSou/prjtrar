/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package projeto;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.dao.CursoDao;
import model.dao.OpcaoDao;
import model.dao.QuestaoDao;
import model.dao.TarefaDao;
import model.dao.impl.CursoDaoImpl;
import model.dao.impl.OpcaoDaoImpl;
import model.dao.impl.QuestaoDaoImpl;
import model.dao.impl.TarefaDaoImpl;
import model.entities.Curso;
import model.entities.Opcao;
import model.entities.Questao;
import model.entities.Tarefa;
import model.enums.Dificuldade;

/**
 *
 * @author Daniel Eneas
 */
public class TELADM2 extends javax.swing.JFrame {

    public TELADM2() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PAINELADM2 = new javax.swing.JTabbedPane();
        INSERIR = new javax.swing.JPanel();
        textinserir = new javax.swing.JLabel();
        textnomexercise = new javax.swing.JLabel();
        botaoConfirmar = new javax.swing.JButton();
        nomexe = new javax.swing.JTextField();
        textnomequestion = new javax.swing.JLabel();
        nomequestion = new javax.swing.JTextField();
        nomeopcion = new javax.swing.JTextField();
        textnomexercise1 = new javax.swing.JLabel();
        nomexe1 = new javax.swing.JTextField();
        textnomexercise2 = new javax.swing.JLabel();
        nomexe2 = new javax.swing.JTextField();
        textnomexercise4 = new javax.swing.JLabel();
        nomexe4 = new javax.swing.JTextField();
        textnomeopcion1 = new javax.swing.JLabel();
        nomeopcion1 = new javax.swing.JTextField();
        textnomeopcion2 = new javax.swing.JLabel();
        textnomeopcion3 = new javax.swing.JLabel();
        nomeopcion2 = new javax.swing.JTextField();
        textnomeopcion4 = new javax.swing.JLabel();
        nomeopcion3 = new javax.swing.JTextField();
        textnomeopcion5 = new javax.swing.JLabel();
        textnomeopcion6 = new javax.swing.JLabel();
        textnomeopcion7 = new javax.swing.JLabel();
        textnomeopcion8 = new javax.swing.JLabel();
        nomeopcion4 = new javax.swing.JTextField();
        nomeopcion5 = new javax.swing.JTextField();
        nomeopcion6 = new javax.swing.JTextField();
        nomeopcion7 = new javax.swing.JTextField();
        textnomeopcion9 = new javax.swing.JLabel();
        nomeopcion8 = new javax.swing.JTextField();
        textnomequestion1 = new javax.swing.JLabel();
        nomexe3 = new javax.swing.JTextField();
        textnomequestion2 = new javax.swing.JLabel();
        nomequestion1 = new javax.swing.JTextField();
        textnomexercise3 = new javax.swing.JLabel();
        nomexe5 = new javax.swing.JTextField();
        TROCAR_TELA1 = new javax.swing.JButton();
        icon = new javax.swing.JLabel();
        wall = new javax.swing.JLabel();
        DELETAR = new javax.swing.JPanel();
        textdelete = new javax.swing.JLabel();
        searchbutton1 = new javax.swing.JButton();
        textdeleteidtask = new javax.swing.JLabel();
        deleteidtask = new javax.swing.JTextField();
        deleteidquestion = new javax.swing.JTextField();
        textdeleteidquestion = new javax.swing.JLabel();
        textdeleteidopcion = new javax.swing.JLabel();
        deleteidopcion = new javax.swing.JTextField();
        wall1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        INSERIR.setBackground(new java.awt.Color(0, 0, 0));
        INSERIR.setLayout(null);

        textinserir.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textinserir.setForeground(new java.awt.Color(242, 242, 242));
        textinserir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textinserir.setText("INSERIR");
        INSERIR.add(textinserir);
        textinserir.setBounds(141, 6, 120, 40);

        textnomexercise.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomexercise.setForeground(new java.awt.Color(242, 242, 242));
        textnomexercise.setText("DIFICULDADE");
        INSERIR.add(textnomexercise);
        textnomexercise.setBounds(6, 89, 148, 22);

        botaoConfirmar.setText("CONFIRMAR");
        botaoConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoConfirmarActionPerformed(evt);
            }
        });
        INSERIR.add(botaoConfirmar);
        botaoConfirmar.setBounds(193, 539, 177, 22);

        nomexe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexeActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe);
        nomexe.setBounds(202, 89, 341, 22);

        textnomequestion.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomequestion.setForeground(new java.awt.Color(242, 242, 242));
        textnomequestion.setText("INSERIR QUESTÃO A TAREFA");
        INSERIR.add(textnomequestion);
        textnomequestion.setBounds(621, 55, 198, 22);

        nomequestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomequestionActionPerformed(evt);
            }
        });
        INSERIR.add(nomequestion);
        nomequestion.setBounds(837, 55, 333, 22);

        nomeopcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcionActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion);
        nomeopcion.setBounds(136, 273, 71, 22);

        textnomexercise1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomexercise1.setForeground(new java.awt.Color(242, 242, 242));
        textnomexercise1.setText("NOME DO TAREFA");
        INSERIR.add(textnomexercise1);
        textnomexercise1.setBounds(6, 55, 126, 22);

        nomexe1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexe1ActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe1);
        nomexe1.setBounds(202, 55, 341, 22);

        textnomexercise2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomexercise2.setForeground(new java.awt.Color(242, 242, 242));
        textnomexercise2.setText("ADIC. AO CURSO");
        INSERIR.add(textnomexercise2);
        textnomexercise2.setBounds(6, 123, 148, 22);

        nomexe2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexe2ActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe2);
        nomexe2.setBounds(202, 123, 341, 22);

        textnomexercise4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomexercise4.setForeground(new java.awt.Color(242, 242, 242));
        textnomexercise4.setText("ENUNCIADO");
        INSERIR.add(textnomexercise4);
        textnomexercise4.setBounds(621, 123, 83, 22);

        nomexe4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexe4ActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe4);
        nomexe4.setBounds(837, 123, 333, 22);

        textnomeopcion1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion1.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion1.setText("INDICE DA OPÇÃO");
        INSERIR.add(textnomeopcion1);
        textnomeopcion1.setBounds(6, 273, 124, 22);

        nomeopcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion1ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion1);
        nomeopcion1.setBounds(387, 273, 341, 22);

        textnomeopcion2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion2.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion2.setText("TEXTO DA OPÇÃO");
        INSERIR.add(textnomeopcion2);
        textnomeopcion2.setBounds(247, 273, 122, 22);

        textnomeopcion3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion3.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion3.setText("INDICE DA OPÇÃO");
        INSERIR.add(textnomeopcion3);
        textnomeopcion3.setBounds(6, 307, 124, 22);

        nomeopcion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion2ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion2);
        nomeopcion2.setBounds(136, 307, 71, 22);

        textnomeopcion4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion4.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion4.setText("TEXTO DA OPÇÃO");
        INSERIR.add(textnomeopcion4);
        textnomeopcion4.setBounds(247, 307, 122, 22);

        nomeopcion3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion3ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion3);
        nomeopcion3.setBounds(387, 307, 341, 22);

        textnomeopcion5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion5.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion5.setText("INDICE DA OPÇÃO");
        INSERIR.add(textnomeopcion5);
        textnomeopcion5.setBounds(6, 341, 124, 22);

        textnomeopcion6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion6.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion6.setText("INDICE DA OPÇÃO");
        INSERIR.add(textnomeopcion6);
        textnomeopcion6.setBounds(6, 375, 124, 22);

        textnomeopcion7.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion7.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion7.setText("TEXTO DA OPÇÃO");
        INSERIR.add(textnomeopcion7);
        textnomeopcion7.setBounds(247, 341, 122, 22);

        textnomeopcion8.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion8.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion8.setText("TEXTO DA OPÇÃO");
        INSERIR.add(textnomeopcion8);
        textnomeopcion8.setBounds(247, 375, 122, 22);

        nomeopcion4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion4ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion4);
        nomeopcion4.setBounds(136, 341, 71, 22);

        nomeopcion5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion5ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion5);
        nomeopcion5.setBounds(136, 375, 71, 22);

        nomeopcion6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion6ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion6);
        nomeopcion6.setBounds(387, 341, 341, 22);

        nomeopcion7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion7ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion7);
        nomeopcion7.setBounds(387, 375, 341, 22);

        textnomeopcion9.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomeopcion9.setForeground(new java.awt.Color(242, 242, 242));
        textnomeopcion9.setText("OPÇÃO CORRETA");
        INSERIR.add(textnomeopcion9);
        textnomeopcion9.setBounds(6, 415, 121, 22);

        nomeopcion8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeopcion8ActionPerformed(evt);
            }
        });
        INSERIR.add(nomeopcion8);
        nomeopcion8.setBounds(133, 415, 71, 22);

        textnomequestion1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomequestion1.setForeground(new java.awt.Color(242, 242, 242));
        textnomequestion1.setText("INSERIR OPÇÕES A QUESTÃO");
        INSERIR.add(textnomequestion1);
        textnomequestion1.setBounds(6, 233, 203, 22);

        nomexe3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexe3ActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe3);
        nomexe3.setBounds(221, 233, 341, 22);

        textnomequestion2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomequestion2.setForeground(new java.awt.Color(242, 242, 242));
        textnomequestion2.setText("ID DA QUESTÃO");
        INSERIR.add(textnomequestion2);
        textnomequestion2.setBounds(621, 89, 108, 22);

        nomequestion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomequestion1ActionPerformed(evt);
            }
        });
        INSERIR.add(nomequestion1);
        nomequestion1.setBounds(837, 89, 333, 22);

        textnomexercise3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textnomexercise3.setForeground(new java.awt.Color(242, 242, 242));
        textnomexercise3.setText("ID DA TAREFA");
        INSERIR.add(textnomexercise3);
        textnomexercise3.setBounds(10, 160, 148, 22);

        nomexe5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomexe5ActionPerformed(evt);
            }
        });
        INSERIR.add(nomexe5);
        nomexe5.setBounds(200, 160, 341, 22);

        TROCAR_TELA1.setText("IR PARA TELA ADM CURSO");
        TROCAR_TELA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TROCAR_TELA1ActionPerformed(evt);
            }
        });
        INSERIR.add(TROCAR_TELA1);
        TROCAR_TELA1.setBounds(1027, 453, 199, 53);

        icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/TEXTITTASK2.png"))); // NOI18N
        INSERIR.add(icon);
        icon.setBounds(1120, 610, 160, 60);

        wall.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/2.jpg"))); // NOI18N
        wall.setPreferredSize(new java.awt.Dimension(1366, 726));
        INSERIR.add(wall);
        wall.setBounds(0, 0, 1370, 760);

        PAINELADM2.addTab("ADICIONAR", INSERIR);

        DELETAR.setBackground(new java.awt.Color(0, 0, 0));
        DELETAR.setLayout(null);

        textdelete.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textdelete.setForeground(new java.awt.Color(242, 242, 242));
        textdelete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textdelete.setText("DELETAR");
        DELETAR.add(textdelete);
        textdelete.setBounds(141, 6, 120, 40);

        searchbutton1.setText("CONFIRMAR");
        searchbutton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchbutton1ActionPerformed(evt);
            }
        });
        DELETAR.add(searchbutton1);
        searchbutton1.setBounds(43, 261, 177, 22);

        textdeleteidtask.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textdeleteidtask.setForeground(new java.awt.Color(242, 242, 242));
        textdeleteidtask.setText("INSIRA O ID DA TAREFA");
        DELETAR.add(textdeleteidtask);
        textdeleteidtask.setBounds(6, 104, 160, 22);

        deleteidtask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteidtaskActionPerformed(evt);
            }
        });
        DELETAR.add(deleteidtask);
        deleteidtask.setBounds(213, 104, 55, 22);

        deleteidquestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteidquestionActionPerformed(evt);
            }
        });
        DELETAR.add(deleteidquestion);
        deleteidquestion.setBounds(213, 144, 55, 22);

        textdeleteidquestion.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textdeleteidquestion.setForeground(new java.awt.Color(242, 242, 242));
        textdeleteidquestion.setText("INSIRA O ID DA QUESTAO");
        DELETAR.add(textdeleteidquestion);
        textdeleteidquestion.setBounds(6, 144, 173, 22);

        textdeleteidopcion.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textdeleteidopcion.setForeground(new java.awt.Color(242, 242, 242));
        textdeleteidopcion.setText("INSIRA O ID DA OPÇÃO");
        DELETAR.add(textdeleteidopcion);
        textdeleteidopcion.setBounds(6, 184, 156, 22);

        deleteidopcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteidopcionActionPerformed(evt);
            }
        });
        DELETAR.add(deleteidopcion);
        deleteidopcion.setBounds(213, 184, 55, 22);

        wall1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/2.jpg"))); // NOI18N
        wall1.setPreferredSize(new java.awt.Dimension(1366, 726));
        DELETAR.add(wall1);
        wall1.setBounds(0, 0, 1370, 760);

        PAINELADM2.addTab("DELETAR", DELETAR);

        getContentPane().add(PAINELADM2);
        PAINELADM2.setBounds(0, 0, 1370, 770);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteidopcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteidopcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteidopcionActionPerformed

    private void deleteidquestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteidquestionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteidquestionActionPerformed

    private void deleteidtaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteidtaskActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteidtaskActionPerformed

    private void searchbutton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchbutton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchbutton1ActionPerformed

    private void TROCAR_TELA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TROCAR_TELA1ActionPerformed
        // TODO add your handling code here:
         this.dispose();
        TELADM i=new TELADM();
        i.setVisible(true);
    }//GEN-LAST:event_TROCAR_TELA1ActionPerformed

    private void nomexe5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexe5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexe5ActionPerformed

    private void nomequestion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomequestion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomequestion1ActionPerformed

    private void nomexe3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexe3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexe3ActionPerformed

    private void nomeopcion8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion8ActionPerformed

    private void nomeopcion7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion7ActionPerformed

    private void nomeopcion6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion6ActionPerformed

    private void nomeopcion5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion5ActionPerformed

    private void nomeopcion4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion4ActionPerformed

    private void nomeopcion3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion3ActionPerformed

    private void nomeopcion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion2ActionPerformed

    private void nomeopcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcion1ActionPerformed

    private void nomexe4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexe4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexe4ActionPerformed

    private void nomexe2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexe2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexe2ActionPerformed

    private void nomexe1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexe1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexe1ActionPerformed

    private void nomeopcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeopcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeopcionActionPerformed

    private void nomequestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomequestionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomequestionActionPerformed

    private void nomexeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomexeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomexeActionPerformed

    private void botaoConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoConfirmarActionPerformed
        try{
            CursoDao cursodao = new CursoDaoImpl();
            TarefaDao tarefadao = new TarefaDaoImpl();
            QuestaoDao questaodao = new QuestaoDaoImpl();
            OpcaoDao opcaodao = new OpcaoDaoImpl();

            Curso c = cursodao.findById(Integer.parseInt(nomexe2.getText()));
            Tarefa t = new Tarefa(nomexe5.getText(), c, Dificuldade.valueOf(nomexe.getText()));
            Questao q = new Questao(Integer.parseInt(nomequestion1.getText()), nomexe4.getText(), nomeopcion8.getText(), t);

            Opcao op1 = new Opcao(nomeopcion.getText(), nomeopcion1.getText(), 0, q);
            Opcao op2 = new Opcao(nomeopcion2.getText(), nomeopcion3.getText(), 0, q);
            Opcao op3 = new Opcao(nomeopcion4.getText(), nomeopcion6.getText(), 0, q);
            Opcao op4 = new Opcao(nomeopcion5.getText(), nomeopcion7.getText(), 0, q);

            q.addOpcoes(op1);
            q.addOpcoes(op2);
            q.addOpcoes(op3);
            q.addOpcoes(op4);

            t.addQuestoes(q);
            tarefadao.insert(t);
            questaodao.insert(q);
            for(Opcao o : q.getOpcoes()){
                opcaodao.insert(o);
            }
            
            JOptionPane.showMessageDialog(null, "Dados Inseridos!");
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados.");
        }

    }//GEN-LAST:event_botaoConfirmarActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TELADM2().setVisible(true);
            }
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel DELETAR;
    private javax.swing.JPanel INSERIR;
    private javax.swing.JTabbedPane PAINELADM2;
    private javax.swing.JButton TROCAR_TELA1;
    private javax.swing.JButton botaoConfirmar;
    private javax.swing.JTextField deleteidopcion;
    private javax.swing.JTextField deleteidquestion;
    private javax.swing.JTextField deleteidtask;
    private javax.swing.JLabel icon;
    private javax.swing.JTextField nomeopcion;
    private javax.swing.JTextField nomeopcion1;
    private javax.swing.JTextField nomeopcion2;
    private javax.swing.JTextField nomeopcion3;
    private javax.swing.JTextField nomeopcion4;
    private javax.swing.JTextField nomeopcion5;
    private javax.swing.JTextField nomeopcion6;
    private javax.swing.JTextField nomeopcion7;
    private javax.swing.JTextField nomeopcion8;
    private javax.swing.JTextField nomequestion;
    private javax.swing.JTextField nomequestion1;
    private javax.swing.JTextField nomexe;
    private javax.swing.JTextField nomexe1;
    private javax.swing.JTextField nomexe2;
    private javax.swing.JTextField nomexe3;
    private javax.swing.JTextField nomexe4;
    private javax.swing.JTextField nomexe5;
    private javax.swing.JButton searchbutton1;
    private javax.swing.JLabel textdelete;
    private javax.swing.JLabel textdeleteidopcion;
    private javax.swing.JLabel textdeleteidquestion;
    private javax.swing.JLabel textdeleteidtask;
    private javax.swing.JLabel textinserir;
    private javax.swing.JLabel textnomeopcion1;
    private javax.swing.JLabel textnomeopcion2;
    private javax.swing.JLabel textnomeopcion3;
    private javax.swing.JLabel textnomeopcion4;
    private javax.swing.JLabel textnomeopcion5;
    private javax.swing.JLabel textnomeopcion6;
    private javax.swing.JLabel textnomeopcion7;
    private javax.swing.JLabel textnomeopcion8;
    private javax.swing.JLabel textnomeopcion9;
    private javax.swing.JLabel textnomequestion;
    private javax.swing.JLabel textnomequestion1;
    private javax.swing.JLabel textnomequestion2;
    private javax.swing.JLabel textnomexercise;
    private javax.swing.JLabel textnomexercise1;
    private javax.swing.JLabel textnomexercise2;
    private javax.swing.JLabel textnomexercise3;
    private javax.swing.JLabel textnomexercise4;
    private javax.swing.JLabel wall;
    private javax.swing.JLabel wall1;
    // End of variables declaration//GEN-END:variables
}
