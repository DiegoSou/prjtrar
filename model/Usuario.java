package model;

public class Usuario {
	private String id;
	private String nome;
	private String email;
	private String senha;
	private String time;
        
	public Usuario() {
		
	}
	public Usuario(String id, String nome, String email, String senha, String time) {
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		this.time = time;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
