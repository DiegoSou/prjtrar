package model.enums;

public enum Dificuldade {

	INICIANTE,
	INTERMEDIARIO,
	AVANCADO;
}
