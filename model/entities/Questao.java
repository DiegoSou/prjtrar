package model.entities;

import java.util.ArrayList;
import java.util.List;

public class Questao {

	private Integer numQuestao;
	private String enunciado;
	private List<Opcao> opcoes = new ArrayList<>();
	private String opcaoCorreta;
	private Tarefa tarefa;
	
	public Questao() {
		
	}

	public Questao(Integer numQuestao, String enunciado, String opcaoCorreta, Tarefa tarefa) {
		this.numQuestao = numQuestao;
		this.enunciado = enunciado;
		this.opcaoCorreta = opcaoCorreta;
		this.tarefa = tarefa;
	}
	
	public Integer getNumQuestao() {
		return numQuestao;
	}

	public void setNumQuestao(Integer numQuestao) {
		this.numQuestao = numQuestao;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public List<Opcao> getOpcoes() {
		return opcoes;
	}

	public void addOpcoes(Opcao op) {
		if(opcoes.size() < 5) {
			opcoes.add(op);
		}else {
			throw new ArrayIndexOutOfBoundsException("[-] Erro. A questão já possui 5 alternativas.");
		}
	}
	
	public void removeOpcoes(int index) {
		if(opcoes.size() >= index) {
			opcoes.remove(index-1);
		}else {
			throw new ArrayIndexOutOfBoundsException("[-] Erro. Indice especificado não corresponde a nenhuma alternativa.");
		}
	}

	public String getOpcaoCorreta() {
		return opcaoCorreta;
	}

	public void setOpcaoCorreta(String opcaoCorreta) {
		this.opcaoCorreta = opcaoCorreta;
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	@Override
	public String toString() {
		return numQuestao + ". " + enunciado + ", Opcoes=" + opcoes + "]";
	}
}
