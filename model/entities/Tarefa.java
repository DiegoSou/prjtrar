package model.entities;

import java.util.HashMap;
import java.util.Map;

import model.enums.Dificuldade;

public class Tarefa {
	
	private String id;
	private Curso curso;
	private Map<Integer, Questao> questoes = new HashMap<>();
	private Dificuldade nivel;
	private boolean concluido;
	
	public Tarefa() {
		
	}
	
	public Tarefa(String id, Curso curso, Dificuldade nivel) {
		this.id = id;
		this.curso = curso;
		this.nivel = nivel;
		this.concluido = false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Curso getCurso() {
		return curso;
	}


	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Map<Integer, Questao> getQuestoes() {
		return questoes;
	}
        
        public void addQuestoes(Questao q) {
		if(this.questoes.size() < 5) {
			questoes.put(q.getNumQuestao(), q);
		}else {
			throw new ArrayIndexOutOfBoundsException("[-] Erro. A questão já possui 5 alternativas.");
		}
	}
	
	public void removeQuestoes(Questao q) {
                questoes.remove(q.getNumQuestao());
	}


	public void setQuestoes(Map<Integer, Questao> questoes) {
		this.questoes = questoes;
	}

	public Dificuldade getNivel() {
		return nivel;
	}


	public void setNivel(String nivel) {
		Dificuldade dificuldade = Dificuldade.valueOf(nivel.toUpperCase());
		this.nivel = dificuldade;
	}


	public int isConcluido() {
		if (this.concluido) {
			return 1;
		}else {
			return 0;
		}
	}


	public void setConcluido(int i) {
		if(i == 1) {
			this.concluido = true;
		}else {
			this.concluido = false;
		}
	}

	@Override
	public String toString() {
		return "Tarefa [id=" + id + ", curso=" + curso + ", nivel=" + nivel + ", concluido="
				+ concluido + "] \n";
	}
}
