package model.entities;

public class Opcao {
	
	private String indice;
	private String texto;
	private boolean marcada;
	private Questao questao;
	
	public Opcao() {
		
	}
	
	public Opcao(String indice, String texto, int marcada, Questao questao) {
		this.indice = indice;
		this.texto = texto;
		this.questao = questao;
		setMarcada(marcada);
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int isMarcada() {
		if(this.marcada) {
			return 1;
		}else {
			return 0;
		}
	}

	public void setMarcada(int i) {
		if(i == 1) {
			this.marcada = true;
		}else {
			this.marcada = false;
		}
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	@Override
	public String toString() {
		return "\n" + indice + ". " + texto + ". [" + marcada + "]";
	}
}
