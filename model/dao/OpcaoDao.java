package model.dao;

import java.util.List;

import model.entities.Opcao;
import model.entities.Questao;

public interface OpcaoDao {
	
	void insert(Opcao obj);
	void update(Opcao obj);
	void deleteById(String id);
	Opcao findById(String id);
	List<Opcao> findByQuestao(Questao q);
	List<Opcao> findAll();
}
