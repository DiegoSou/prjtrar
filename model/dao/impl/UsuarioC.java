package model.dao.impl;
import control.db.ConexaoDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.entities.Usuario;
    
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


public class UsuarioC {
    
    public UsuarioC(){
        
    }
    
    public boolean logar(String nome, String senha){
       PreparedStatement ps;
       Usuario b = new Usuario();
       
       try{
            boolean c = ConexaoDb.abrirConexao();
            ps = ConexaoDb.getConn().prepareStatement("SELECT * FROM user WHERE nome = ? AND senha = ?");
            ps.setString(1, nome);
            ps.setString(2, senha);
            ResultSet rs = ps.executeQuery();
            if(rs != null) { 
                while(rs.next()){
                    b.setNome(rs.getString(1));
                    b.setSenha(rs.getString(2));
                    return true;
                }
            }
       }catch(SQLException e){
           JOptionPane.showMessageDialog(null, "Erro ao executar login " + e.getMessage());
           return false;
       }
       return false; 
    }
    
    public boolean cadastrar(Usuario usuario){
        PreparedStatement ps;
        
        try{
            boolean c = ConexaoDb.abrirConexao();
            ps = ConexaoDb.getConn().prepareStatement("INSERT INTO user (id, nome, email, senha, time) VALUES(?, ?, ? ,?, ?);");

            ps.setInt(1, usuario.getId());
            ps.setString(2, usuario.getNome());
            ps.setString(3, usuario.getEmail());
            ps.setString(4, usuario.getSenha());
            ps.setString(5, "comum");

            int rs = ps.executeUpdate();
            if(rs == 1){
                return true;
            }else{
                return false;
            }
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao executar registro. - " + e.getMessage(), "Erro", 2);
            return false;
        }finally{
            ConexaoDb.fecharConexao();
        }
    }

    
    public List<Usuario> buscar(){
        PreparedStatement ps;
        ResultSet rs;
        
        try{
            boolean c = ConexaoDb.abrirConexao();
            ps = ConexaoDb.getConn().prepareStatement("SELECT * FROM user");
            rs = ps.executeQuery();
            
            List<Usuario> users = new ArrayList<>();
            while(rs.next()){
                Usuario u = new Usuario(rs.getInt("id"), rs.getString("nome"), rs.getString("email"), rs.getString("senha"), rs.getString("time"));
                users.add(u);
            }
            
            return users;
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao executar registro. - " + e.getMessage(), "Erro", 2);
            return null;
        } finally {
            ConexaoDb.fecharConexao();
        }
    }
    
    public boolean update(Usuario usu){
        PreparedStatement ps;
        
        try{
            boolean c = ConexaoDb.abrirConexao();
            ps = ConexaoDb.getConn().prepareStatement("SELECT * FROM user WHERE nome = ? AND senha = ?");
            ps.setString(1, usu.getNome());
            ps.setString(2, usu.getSenha());
            
            int linhasAfet = ps.executeUpdate();
            if(linhasAfet > 0){
                return true;
            }else{
                throw new SQLException("Nenhum atualizado");
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao executar registro. - " + e.getMessage(), "Erro", 2);
            return false;
        } finally {
            ConexaoDb.fecharConexao();
        }
    }
    
    public String excluirConta(Usuario usuario){
        PreparedStatement ps;
        
        try{
            boolean c = ConexaoDb.abrirConexao();
            ps = ConexaoDb.getConn().prepareStatement("DELETE FROM user WHERE id = ? AND nome = ?");
            ps.setInt(1, usuario.getId());
            ps.setString(2, usuario.getNome());
            
            if(ps.executeUpdate() > 0){
                return "Excluido com sucesso";
            } else {
                return "Erro ao excluir";
            }
        }catch(SQLException e){
            return e.getMessage(); 
                    
        }finally{
            ConexaoDb.fecharConexao();
        }    
    }
}