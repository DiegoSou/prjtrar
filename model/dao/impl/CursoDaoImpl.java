package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import control.db.ConexaoDb;
import control.db.ConexaoException;
import model.dao.CursoDao;
import model.entities.Curso;

public class CursoDaoImpl implements CursoDao {

	private Connection conn;
	
        public CursoDaoImpl(){
                boolean c = ConexaoDb.abrirConexao();
                this.setConn(ConexaoDb.getConn());
        }
        
	private void setConn(Connection conn) {
		this.conn = conn;
	}

	@Override
	public void insert(Curso obj) {
                System.out.println(conn);
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(
					"INSERT INTO curso(nome, descricao) "
					+ "VALUES(?,?)",
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, obj.getNome());
			ps.setString(2, obj.getDescricao());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next()) {
					int id = rs.getInt(1);
					obj.setId(id);
					
					System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
				}
			}else {
				throw new SQLException("[-] Erro ao inserir dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
		
	}

	@Override
	public void update(Curso obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(
					"UPDATE curso SET nome=?, descricao=?"
					+ " WHERE id=?");
			
			ps.setString(1, obj.getNome());
			ps.setString(2, obj.getDescricao());
			ps.setInt(3, obj.getId());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public void deleteById(Integer id) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("DELETE FROM curso WHERE id=?");
			
			ps.setInt(1, id);
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao deletar dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public Curso findById(Integer id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement("SELECT * FROM curso WHERE id=?");
			
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				Curso curso = instantiateCurso(rs);
				return curso;
			}else {
				System.out.println("[-] Nenhum curso com este identificador encontrado.");
				return null;
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
			ConexaoDb.fecharResultSet(rs);
		}
	}

	@Override
	public List<Curso> findAll() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Curso> cursos = new ArrayList<>();
		
		try {
			ps = conn.prepareStatement("SELECT * FROM curso");
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				Curso obj = instantiateCurso(rs);
				cursos.add(obj);
			}
			
			return cursos;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	public Curso instantiateCurso(ResultSet obj) throws SQLException {
		Curso c = new Curso();
		c.setId(obj.getInt("id"));
		c.setNome(obj.getString("nome"));
		c.setDescricao(obj.getString("descricao"));
		
		return c;
	}
}
