package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import control.db.ConexaoDb;
import control.db.ConexaoException;
import model.dao.TarefaDao;
import model.entities.Curso;
import model.entities.Tarefa;

public class TarefaDaoImpl implements TarefaDao {

	private Connection conn;
	
	public TarefaDaoImpl() {
		boolean c = ConexaoDb.abrirConexao();
                this.setConn(ConexaoDb.getConn());
	}
        
        private void setConn(Connection conn){
            this.conn = conn;
        }

	@Override
	public void insert(Tarefa obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("INSERT INTO tarefa(id, dificuldade, curso) VALUES(?,?,?)");
			
			ps.setString(1, obj.getId());
			ps.setString(2, obj.getNivel().toString());
			ps.setInt(3, obj.getCurso().getId());
			
			int linhasAfet = ps.executeUpdate();
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}	
	}

	@Override
	public void update(Tarefa obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("UPDATE tarefa SET dificuldade=?, curso=?, concluida=? WHERE id=?");
			
			ps.setString(1, obj.getNivel().toString());
			ps.setInt(2, obj.getCurso().getId());
			ps.setInt(3, obj.isConcluido());
			ps.setString(4, obj.getId());
			
			int linhasAfet = ps.executeUpdate();
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public void deleteById(String id) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("DELETE FROM tarefa WHERE id=?");
			
			ps.setString(1, id);
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public Tarefa findById(String id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT tarefa.id, tarefa.dificuldade, tarefa.concluida, tarefa.curso, curso.nome AS NomeCurso"
					+ " FROM tarefa"
					+ " INNER JOIN curso ON tarefa.curso = curso.id"
					+ " WHERE tarefa.id = ?");
			
			ps.setString(1, id);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				Curso c = instantiateCurso(rs);
				Tarefa t = instantiateTarefa(rs, c);
				return t;
			}
			return null;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public List<Tarefa> findByCurso(Curso obj) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT tarefa.id, tarefa.dificuldade, tarefa.concluida, tarefa.curso, curso.nome AS NomeCurso"
					+ " FROM tarefa"
					+ " INNER JOIN curso ON tarefa.curso = curso.id"
					+ " WHERE tarefa.curso = ?");
			
			ps.setInt(1, obj.getId());
			rs = ps.executeQuery();
			
			List<Tarefa> lista = new ArrayList<>();
			Map<Integer, Curso> cursos = new HashMap<>();
			
			while(rs.next()) {
				Curso c = cursos.get(rs.getInt("curso"));
				
				if(c == null) {
					c = instantiateCurso(rs);
					cursos.put(rs.getInt("curso"), c);
				}
				
				Tarefa t = instantiateTarefa(rs, c);
				lista.add(t);
			}
			return lista;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}
	
	@Override
	public List<Tarefa> findAll() {
		PreparedStatement ps = null;
		ResultSet rs = null;
				
		try {
			ps = conn.prepareStatement(
					"SELECT tarefa.id, tarefa.dificuldade, tarefa.concluida, tarefa.curso, curso.nome AS NomeCurso"
					+ " FROM tarefa"
					+ " INNER JOIN curso ON tarefa.curso = curso.id"
					+ " ORDER BY NomeCurso");
			
			rs = ps.executeQuery();
			
			List<Tarefa> lista = new ArrayList<>();
			Map<Integer, Curso> cursos = new HashMap<>();
			
			while(rs.next()) {
				Curso c = cursos.get(rs.getInt("curso"));
				
				if(c == null) {
					c = instantiateCurso(rs);
					cursos.put(rs.getInt("curso"), c);
				}
				
				Tarefa t = instantiateTarefa(rs, c);
				lista.add(t);
			}
			return lista;
		}
		catch(SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}
	
	public Curso instantiateCurso(ResultSet obj) throws SQLException {
		Curso c = new Curso();
		c.setId(obj.getInt("curso"));
		c.setNome(obj.getString("NomeCurso"));
		
		return c;
	}
	
	public Tarefa instantiateTarefa(ResultSet obj, Curso c) throws SQLException {
		Tarefa t = new Tarefa();
		t.setId(obj.getString("id"));
		t.setNivel(obj.getString("dificuldade"));
		t.setConcluido(obj.getInt("concluida"));
		t.setCurso(c);
		
		return t;
	}
}
