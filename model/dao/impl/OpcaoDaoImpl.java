package model.dao.impl;

import control.db.ConexaoDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import control.db.ConexaoException;
import model.dao.OpcaoDao;
import model.entities.Opcao;
import model.entities.Questao;

public class OpcaoDaoImpl implements OpcaoDao {
	
	private Connection conn;
	
	public OpcaoDaoImpl() {
		boolean c = ConexaoDb.abrirConexao();
                this.setConn(ConexaoDb.getConn());
	}
        
        private void setConn(Connection conn){
            this.conn = conn;
        }

	@Override
	public void insert(Opcao obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("INSERT INTO opcao(indice, texto, questao) VALUES(?,?,?)");
			
			ps.setString(1, obj.getIndice());
			ps.setString(2, obj.getTexto());
			ps.setInt(3, obj.getQuestao().getNumQuestao());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	@Override
	public void update(Opcao obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("UPDATE opcao SET marcada=? WHERE indice=?");
			
			ps.setInt(1, obj.isMarcada());
			ps.setString(2, obj.getIndice());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	@Override
	public void deleteById(String id) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("DELETE FROM opcao WHERE indice=?");
			ps.setString(1, id);
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	@Override
	public Opcao findById(String id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT opcao.indice, opcao.texto, opcao.marcada, questao.id AS NumQuestao"
					+ " FROM opcao"
					+ " INNER JOIN questao ON opcao.questao = questao.id"
					+ " WHERE opcao.indice = ?");
			
			ps.setString(1, id);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				Questao q = instantiateQuestao(rs);
				Opcao op = instantiateOpcao(rs, q);
				return op;
			}
			return null;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	@Override
	public List<Opcao> findByQuestao(Questao obj) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT opcao.indice, opcao.texto, opcao.marcada, questao.id AS NumQuestao"
					+ " FROM opcao"
					+ " INNER JOIN questao ON opcao.questao = questao.id"
					+ " WHERE questao.id = ?"
					+ " ORDER BY opcao.indice");
			
			ps.setInt(1, obj.getNumQuestao());
			rs = ps.executeQuery();
			
			List<Opcao> lista = new ArrayList<>();
			Map<Integer, Questao> questoes = new HashMap<>();
			
			while(rs.next()) {
				Questao q = questoes.get(rs.getInt("NumQuestao"));
				
				if(q == null) {
					q = instantiateQuestao(rs);
					questoes.put(rs.getInt("NumQuestao"), q);
				}
				
				Opcao op = instantiateOpcao(rs, q);
				lista.add(op);
			}
			return lista;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}

	@Override
	public List<Opcao> findAll() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT opcao.indice, opcao.texto, opcao.marcada, questao.id AS NumQuestao"
					+ " FROM opcao"
					+ " INNER JOIN questao ON opcao.questao = questao.id"
					+ " ORDER BY opcao.indice");
			
			rs = ps.executeQuery();
			List<Opcao> lista = new ArrayList<>();
			Map<Integer, Questao> questoes = new HashMap<>();
			
			while(rs.next()) {
				Questao q = questoes.get(rs.getInt("NumQuestao"));
				
				if(q == null) {
					q = instantiateQuestao(rs);
					questoes.put(rs.getInt("NumQuestao"), q);
				}
				
				Opcao op = instantiateOpcao(rs, q);
				lista.add(op);
			}
			return lista;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}
	
	public Questao instantiateQuestao(ResultSet obj) throws SQLException {
		Questao q = new Questao();
		q.setNumQuestao(obj.getInt("NumQuestao"));
		
		return q;
	}
	
	public Opcao instantiateOpcao(ResultSet obj, Questao q) throws SQLException {
		Opcao op = new Opcao();
		op.setIndice(obj.getString("indice"));
		op.setTexto(obj.getString("texto"));
		op.setMarcada(obj.getInt("marcada"));
		op.setQuestao(q);
		
		return op;
	}
	
}
