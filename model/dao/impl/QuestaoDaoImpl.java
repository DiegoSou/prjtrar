package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import control.db.ConexaoDb;
import control.db.ConexaoException;
import model.dao.QuestaoDao;
import model.entities.Questao;
import model.entities.Tarefa;

public class QuestaoDaoImpl implements QuestaoDao {

	private Connection conn;
	
	public QuestaoDaoImpl() {
		boolean c = ConexaoDb.abrirConexao();
                this.setConn(ConexaoDb.getConn());
	}
       
        private void setConn(Connection conn){
            this.conn = conn;
        }

	@Override
	public void insert(Questao obj) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement("INSERT INTO questao(enunciado, opcaoCorreta, tarefa) VALUES(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, obj.getEnunciado());
			ps.setString(2, obj.getOpcaoCorreta());
			ps.setString(3, obj.getTarefa().getId());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				rs = ps.getGeneratedKeys();
				if(rs.next()) {
					obj.setNumQuestao(rs.getInt(1));
				}
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public void update(Questao obj) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("UPDATE questao SET enunciado=?, opcaoCorreta=?, tarefa=? WHERE id=?");
			
			ps.setString(1, obj.getEnunciado());
			ps.setString(2, obj.getOpcaoCorreta());
			ps.setString(3, obj.getTarefa().getId());
			ps.setInt(4, obj.getNumQuestao());
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public void deleteById(Integer id) {
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement("DELETE FROM questao WHERE id=?");
			
			ps.setInt(1, id);
			
			int linhasAfet = ps.executeUpdate();
			
			if(linhasAfet > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + linhasAfet);
			}else {
				throw new SQLException("[-] Erro ao atualizar os dados. Nenhuma linha afetada");
			}
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public Questao findById(Integer id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT questao.id, questao.enunciado, questao.opcaoCorreta, questao.tarefa, tarefa.dificuldade"
					+ " FROM questao"
					+ " INNER JOIN tarefa ON questao.tarefa = tarefa.id"
					+ " WHERE questao.id = ?");
			
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				Tarefa t = instantiateTarefa(rs);
				Questao q = instantiateQuestao(rs, t);
				return q;
			}
			return null;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}

	@Override
	public List<Questao> findByTarefa(Tarefa tf) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(
					"SELECT questao.id, questao.enunciado, questao.opcaoCorreta, questao.tarefa, tarefa.dificuldade"
					+ " FROM questao"
					+ " INNER JOIN tarefa ON questao.tarefa = tarefa.id"
					+ " WHERE questao.tarefa = ?");
			
			ps.setString(1, tf.getId());
			
			rs = ps.executeQuery();
			List<Questao> questoes = new ArrayList<>();
			Map<String, Tarefa> tarefas = new HashMap<>();
			
			while(rs.next()) {
				Tarefa t = tarefas.get(rs.getString("tarefa"));
				
				if(t == null) {
					t = instantiateTarefa(rs);
					tarefas.put(rs.getString("tarefa"), t);
				}
				
				Questao q = instantiateQuestao(rs, t);
				questoes.add(q);
			}
			return questoes;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}
	
	@Override
	public List<Questao> findAll() {
		PreparedStatement ps = null;
		ResultSet rs = null;
				
		try {
			ps = conn.prepareStatement(
					"SELECT questao.id, questao.enunciado, questao.opcaoCorreta, questao.tarefa, tarefa.dificuldade"
					+ " FROM questao"
					+ " INNER JOIN tarefa ON questao.tarefa = tarefa.id");
			
			
			rs = ps.executeQuery();
			List<Questao> questoes = new ArrayList<>();
			Map<String, Tarefa> tarefas = new HashMap<>();
			
			while(rs.next()) {
				Tarefa t = tarefas.get(rs.getString("tarefa"));
				
				if(t == null) {
					t = instantiateTarefa(rs);
					tarefas.put(rs.getString("tarefa"), t);
				}
				
				Questao q = instantiateQuestao(rs, t);
				questoes.add(q);
			}
			return questoes;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
		finally {
			ConexaoDb.fecharResultSet(rs);
			ConexaoDb.fecharStatement(ps);
		}
	}
	
	public Tarefa instantiateTarefa(ResultSet obj) throws SQLException {
		Tarefa t = new Tarefa();
		t.setId(obj.getString("tarefa"));
		t.setNivel(obj.getString("dificuldade"));
		
		return t;
	}
	
	public Questao instantiateQuestao(ResultSet obj, Tarefa t) throws SQLException {
		Questao q = new Questao();
		q.setNumQuestao(obj.getInt("id"));
		q.setEnunciado(obj.getString("enunciado"));
		q.setOpcaoCorreta(obj.getString("opcaoCorreta"));
		q.setTarefa(t);
		
		return q;
	}
}
