package model.dao;

import java.util.List;

import model.entities.Curso;
import model.entities.Tarefa;

public interface TarefaDao {

	void insert(Tarefa obj);
	void update(Tarefa obj);
	void deleteById(String id);
	Tarefa findById(String id);
	List<Tarefa> findByCurso(Curso obj);
	List<Tarefa> findAll();
}
