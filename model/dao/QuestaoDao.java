package model.dao;

import java.util.List;

import model.entities.Questao;
import model.entities.Tarefa;

public interface QuestaoDao {

	void insert(Questao obj);
	void update(Questao obj);
	void deleteById(Integer id);
	Questao findById(Integer id);
	List<Questao> findByTarefa(Tarefa tf);
	List<Questao> findAll();
}
